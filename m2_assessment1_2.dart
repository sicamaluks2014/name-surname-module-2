List<Winner> winners = [];

void main() {
  winners.add(Winner("FNB", 2012));
  winners.add(Winner("SnapScan", 2013));
  winners.add(Winner("Supersports", 2014));
  winners.add(Winner("Dstv Now", 2014));
  winners.add(Winner("Wumdrop", 2015));
  winners.add(Winner("PicknPay", 2016));
  winners.add(Winner("Shyft", 2017));
  winners.add(Winner("Khula ecosystem", 2018));
  winners.add(Winner("Nakek Insurance", 2019));
  winners.add(Winner("EasyEquities", 2020));
  winners.add(Winner("Ambani Africa", 2021));

  //Sort and print the apps by name
  winners.sort((a, b) => (a.name).compareTo(b.name));
  for (var element in winners) {
    print(element.name);
  }

  //Print the winning app of 2017 and the winning app of 2018
  for (var x in winners) {
    if (x.year == 2017 || x.year == 2018) {
      print("Winner for " + x.year.toString() + " was " + x.name);
    }
  }

  //print total number of apps from the array
  print(winners.length);
}

class Winner {
  final String name;
  final int year;

  Winner(this.name, this.year);
}
