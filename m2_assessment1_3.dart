class Winner {
  final String name;
  final String sector;
  final String developer;
  final int year;

  Winner(this.name, this.sector, this.developer, this.year);

  String capitalizeName(String name) {
    return name.toUpperCase();
  }
}

void main() {
  final Winner winner =
      Winner("Ambani Africa", "Education", "Ambani Africa", 2021);
  //print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards.
  print(winner.name +
      "\n" +
      winner.sector +
      "\n" +
      winner.developer +
      "\n" +
      winner.year.toString());

  //print capitalized app name
  print(winner.capitalizeName(winner.name));
}
