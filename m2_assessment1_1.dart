void main() {
  Person me = Person("Simon", "YouTube", "Pretoria");

  print(me.name);
  print(me.favoriteApp);
  print(me.city);
}

class Person {
  final String name;
  final String favoriteApp;
  final String city;

  Person(this.name, this.favoriteApp, this.city);
}
